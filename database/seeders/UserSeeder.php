<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User ::create([
            'name' => 'edwinAdmin',
            'email' => 'edwin@caed.com',
            'password' =>  bcrypt('abc1234'),
        ]);
    }
}
