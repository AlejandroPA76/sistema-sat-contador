 @extends('layouts.app')
 @section('content')
  <div class="ibox">
     <div class="ibox-head">
                        <div class="ibox-title">Agregar Municipios</div>
                    </div>
 <div class="ibox-body">
    <!-- Button trigger modal -->
                  <button type="button" class="btn btn-success" data-toggle="modal" data-target="#addMunicipio" >
    Agregar Municipio
</button> 

                      <!-- Modal -->
<div class="modal fade" id="addMunicipio" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Agregar datos del municipio</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <div class="ibox-body">
       <form class="form-horizontal" action="municipios" method="post">
               @csrf            
                  <div class="form-group row">
                 <label class="col-sm-10 col-form-label">Nombre del Municipio:</label>
                  <div class="col-sm-10">
                   <input class="form-control" name="nombre" type="text" value="" required>
                         </div>
                        </div>

           <div class="form-group row">
                
                  <label class="col-sm-10 col-form-label">Selecciona el estado:</label>
                            <div class="col-sm-10">
                                        <select class="form-control" name="estado">
                                            @foreach($estados as $est)
                                            <option value="{{$est->id}}">{{$est->nombre}}</option>
                                            @endforeach
                                        </select>

                                   </div>
                        </div>  
                        <br>                      
                                  
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Agregar</button>
      </div>
                                  
                                </form>
                            </div>
      </div>
     
    </div>
  </div>
</div> 
<!-- fin modal-->
                    </div>
                </div></div>


 <div class="ibox">
                    <div class="ibox-head">
                        <div class="ibox-title">Listado de Municipios</div>
                    </div>
                    <div class="ibox-body">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>                                       
                                        <th>Id</th>
                                        <th>Nombre</th>  
                                        <th>Estado</th>                                     
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($municipios as $mu)
                                    <tr>
                                       
                                        
                                        <td>{{$mu->id}}</td>
                                      <td>{{$mu->nombre}}</td>
                                      <td>{{$mu->estado->nombre}}</td>
                                        <td>
     <!-- Button trigger modal -->
   <button type="button" class="btn btn-default btn-xs" data-toggle="modal" data-target="#editMunicipio{{$mu->id}}" >
    <i class="fa fa-pencil font-14"></i>
</button>

                      <!-- Modal -->
<div class="modal fade" id="editMunicipio{{$mu->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Editar los datos del municipio</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <div class="ibox-body">
       <form class="form-horizontal" action="municipios/{{$mu->id}}}" method="post">
               @csrf
                @method('put')          
                  <div class="form-group row">
                 <label class="col-sm-10 col-form-label">Nombre del Municipio:</label>
                  <div class="col-sm-10">
                   <input class="form-control" name="nombre" type="text" value="{{$mu->nombre}}" required>
                         </div>
                        </div>

           <div class="form-group row">
                
                  <label class="col-sm-10 col-form-label">Selecciona el estado:</label>
                            <div class="col-sm-10">
                                        <select class="form-control" name="estado">
                                        <option value="{{$mu->estado->id}}" selected>{{$mu->estado->nombre}}</option>
                                            @foreach($estados as $est)
                                            <option value="{{$est->id}}">{{$est->nombre}}</option>
                                            @endforeach
                                        </select>

                                   </div>
                        </div>  
                        <br>                      
                                  
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Agregar</button>
      </div>
                                  
                                </form>
                            </div>
      </div>
     
    </div>
  </div>
</div> 
<!-- fin modal-->


 <a href="javascript: document.getElementById('delete{{$mu->id}}').submit()" class="btn btn-default btn-xs" data-toggle="tooltip" data-original-title="Eliminar"
 onclick="return confirm('deseas borrar?')"> <i class="fa fa-trash font-14"></i></a>

                                                  <form id="delete{{$mu->id}}" action="municipios/{{$mu->id}}" method="POST">
                                                    @csrf
                                                    @method('delete')
                                                   
                                                  </form>
                                        </td>
                                      
                                    </tr>
                                    @endforeach
                                
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>                
@endsection                