 @extends('layouts.app')
 @section('content')  <div class="ibox">
     <div class="ibox-head">
                        <div class="ibox-title">Registrar clientes</div>
                        <div>
                          
                        </div>
                    </div>
 <div class="ibox-body">
    <!-- Button trigger modal -->
                  <button type="button" class="btn btn-success" data-toggle="modal" data-target="#addMunicipio" >
    Agregar Cliente
</button> 

                      <!-- Modal -->
<div class="modal fade" id="addMunicipio" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Registrar cliente</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <div class="ibox-body">
       <form class="form-horizontal" action="clientes" method="post" enctype="multipart/form-data">
               @csrf            
                  <div class="form-group row">
                 <label class="col-sm-10 col-form-label">Nombre del Cliente:</label>
                  <div class="col-sm-10">
                   <input class="form-control" name="nombre" type="text" value="" required>
                         </div>
                        </div>

           <div class="form-group row">
                
                  <label class="col-sm-10 col-form-label">Selecciona el regimen:</label>
                            <div class="col-sm-10">
                                        <select class="form-control" name="regimen">
                                           @foreach($regimens as $re)
                                            <option value="{{$re->id}}">{{$re->tipo}}</option>
                                           @endforeach
                                        </select>

                                   </div>
                        </div> 

            <div class="form-group row">
                
                  <label class="col-sm-10 col-form-label">Municipio/Estado:</label>
                            <div class="col-sm-10">
                                        <select class="form-control" name="region">
                                           @foreach($regions as $region)
                                            <option value="{{$region->id}}">{{$region->nombre}}-{{$region->estado->nombre}}</option>
                                           @endforeach
                                        </select>

                                   </div>
                        </div>

                           <div class="form-group">
    <label for="exampleFormControlTextarea1">Observacion:</label>
    <textarea class="form-control" name="observacion" rows="3"></textarea>
  </div>

                        <div class="mb-3">
  <label for="formFileMultiple" class="form-label">Agrega los archivos</label>
  <input class="form-control" type="file" name="archivos[]" multiple>
</div>

                        <br>                      
                                  
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Agregar</button>
      </div>
                                  
                                </form>
                            </div>
      </div>
     
    </div>
  </div>
</div> 
<!-- fin modal-->
                    </div>
                </div></div>


 <div class="ibox">
                    <div class="ibox-head">
                        <div class="ibox-title">Listado de Clientes</div>
                    </div>
                    <div class="ibox-body">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>

                                    <tr>                                       
                                        <th>Id</th>
                                        <th>Nombre</th>  
                                        <th>Regimen</th> 
                                        <th>Region</th>                                    
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($clientes as $cli)
                                    <tr>
                                       
                                        
                                      <td>{{$cli->id}}</td>
                                      <td>{{$cli->nombre}}</td>
                                      <td>{{$cli->regimen->tipo}}</td>
                                      <td>{{$cli->municipio->nombre}}-{{$cli->municipio->estado->nombre}}</td>
                    
                                        <td>
                <!--Menu ver Button trigger modal -->
   <button type="button" class="btn btn-default btn-xs" data-toggle="modal" data-target="#showCliente{{$cli->id}}"  >
    <i class="fa ti-eye font-14"></i>
    </button>

                      <!-- Modal -->
<div class="modal fade" id="showCliente{{$cli->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Datos del Cliente</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <div class="ibox-body">
       <form class="form-horizontal" action="clientes/{{$cli->id}}" method="post">
               @csrf
                @method('put')          
                  <div class="form-group row">
                 <label class="col-sm-10 col-form-label">Nombre del Cliente:</label>
                  <div class="col-sm-10">
                   <input class="form-control" name="nombre" type="text" value="{{$cli->nombre}}" required disabled>
                         </div>
                        </div>

           <div class="form-group row">            
                <label class="col-sm-10 col-form-label">Regimen:</label>
                    <div class="col-sm-10">
                            <select class="form-control" name="regimen" disabled>
                            <option value="{{$cli->regimen->id}}" selected>{{$cli->regimen->tipo}}</option>
                                        
                                        </select>

                                   </div>
                        </div>  


         <div class="form-group row">            
                <label class="col-sm-10 col-form-label">Region:</label>
                    <div class="col-sm-10">
                            <select class="form-control" name="region" disabled>
                            <option value="{{$cli->municipio->id}}" selected>{{$cli->municipio->nombre}}-{{$cli->municipio->estado->nombre}}</option>
                                         
                                        </select>

                                   </div>
                        </div>                        
                          <div class="form-group">
                            <label for="exampleFormControlTextarea1">Observacion:</label>
                            <textarea class="form-control" name="observacion" rows="3" disabled>{{$cli->observacion}}</textarea >
                          </div>

                <div class="form-group row">
                 <label class="col-sm-10 col-form-label">Archivos del Cliente:</label>
                  <div class="col-sm-10">

                    @foreach($cli->archivo as $ar)
                
                 <a href="/showFiles/{{$ar->id}}" class="btn btn-link">{{$ar->nombre}}</a>        
                    @endforeach

                  
                         </div>
                        </div>
                          
                        <br>                      
                                  
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
       
      </div>
                                  
                                </form>
                            </div>
      </div>
     
    </div>
  </div>
</div>  
       <!-- fin-->


     <!--Menu de editar Button trigger modal -->
   <button type="button" class="btn btn-default btn-xs" data-toggle="modal" data-target="#editMunicipio{{$cli->id}}" >
    <i class="fa fa-pencil font-14"></i>
</button>

                      <!-- Modal -->
<div class="modal fade" id="editMunicipio{{$cli->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Editar los datos del Cliente</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <div class="ibox-body">
       <form class="form-horizontal" action="clientes/{{$cli->id}}" method="post" enctype="multipart/form-data">
               @csrf
                @method('put')          
                  <div class="form-group row">
                 <label class="col-sm-10 col-form-label">Nombre del Cliente:</label>
                  <div class="col-sm-10">
                   <input class="form-control" name="nombre" type="text" value="{{$cli->nombre}}" required>
                         </div>
                        </div>

           <div class="form-group row">            
                <label class="col-sm-10 col-form-label">Selecciona el Regimen:</label>
                    <div class="col-sm-10">
                            <select class="form-control" name="regimen">
                            <option value="{{$cli->regimen->id}}" selected>{{$cli->regimen->tipo}}</option>
                                          @foreach($regimens as $re)
                                            <option value="{{$re->id}}">{{$re->tipo}}</option>
                                         @endforeach
                                        </select>

                                   </div>
                        </div>  


         <div class="form-group row">            
                <label class="col-sm-10 col-form-label">Selecciona la region:</label>
                    <div class="col-sm-10">
                            <select class="form-control" name="region">
                            <option value="{{$cli->municipio->id}}" selected>{{$cli->municipio->nombre}}-{{$cli->municipio->estado->nombre}}</option>
                                          @foreach($regions as $region)
                                            <option value="{{$region->id}}">{{$region->nombre}}-{{$region->estado->nombre}}</option>
                                          @endforeach
                                        </select>

                                   </div>
                        </div>                        
                          <div class="form-group">
                            <label for="exampleFormControlTextarea1">Observacion:</label>
                            <textarea class="form-control" name="observacion" rows="3">{{$cli->observacion}}</textarea>
                          </div>

                   
                        <div class="mb-3">
              <label for="formFileMultiple" class="form-label">Agrega los archivos(Nota: los archivos con el mismo nombre se reemplazara por el mas nuevo)</label>
              <input class="form-control" type="file" name="archivos[]" multiple  >
            </div>


                        <br>                      
                                  
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Actualizar</button>
      </div>
                                  
                                </form>

                                  <div class="form-group row">  
                         @foreach($cli->archivo as $ar)
                          <div class="col-sm-10" style="margin-bottom: 7px;">                      

                 <form id= "h" action="{{route('delfi',$ar->id)}}" method="POST">
                 @csrf
                 @method('delete')
                 <input type="submit" bna class="btn btn-danger btn-sm" name="id" value="{{$ar->nombre}}" onclick="return confirm('deseas borrar?')" >
                <input type="text" name="model" value="#editMunicipio{{$cli->id}}" hidden>
                </form>
                           
                      </div>


                     
                    @endforeach
            
                    
                        </div>
                            </div>
      </div>
     
    </div>
  </div>
</div> 
<!-- fin modal-->


 <a href="javascript: document.getElementById('delete{{$cli->id}}').submit()" class="btn btn-default btn-xs" data-toggle="tooltip" data-original-title="Eliminar"
 onclick="return confirm('deseas borrar?')"> <i class="fa fa-trash font-14"></i></a>

                                                  <form id="delete{{$cli->id}}" action="clientes/{{$cli->id}}" method="POST">
                                                    @csrf
                                                    @method('delete')
                                                   
                                                  </form>
                                        </td>
                                      
                                    </tr>
                                   @endforeach
                                
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>                
@endsection                


