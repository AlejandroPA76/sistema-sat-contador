@extends('layouts.app')
@section('content')

<!--guiarse de este proyecto
    vista/contandor/historialDePagos
https://github.dev/AlejandroPA76/controlEscolar
 -->
 <div class="ibox">
        <div class="card">
                            <form action="#" method="get" autocomplete="off">

                                <div class="container">
                                    <div class="row align-items-center">
                                        <div class="col">
    
                                            <label>Buscar por mes</label>
                                            <input type="month" name="fecha" min="2021-01" >
                                            <button type="submit" class="btn btn-dark">Buscar</button>
                                            
                                        </div>
                                        <br>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <br>                

                         <div class="ibox">
                    <div class="ibox-head">
                        <div class="ibox-title">Listado de Pagos</div>
                    </div>
                    <div class="ibox-body">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>                                       
                                        <th>Id</th>
                                        <th>Nombre</th>  
                                     
                                    </tr>
                                </thead>
                                <tbody>
                                   @foreach($clientesPago as $cp)
                                        <td>{{$cp->id}}</td>
                                        <td>{{$cp->nombre}}</td>
                                    @endforeach                                
                                </tbody>
                            </table>
                        </div>
             </div> </div>
 </div>
@endsection