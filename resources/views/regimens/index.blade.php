 @extends('layouts.app')
 @section('content')
  <div class="ibox">
     <div class="ibox-head">
                        <div class="ibox-title">Agregar Regimen fiscal</div>
                    </div>
 <div class="ibox-body">
                   

                        <form class="form-inline" action="regimens" method="post">
                           @csrf
                            <input class="form-control mb-2 mr-sm-2 mb-sm-0 " name = "name"type="text" placeholder="Nombre del regimen" required>
        
                            <button class="btn btn-success" type="submit">Agregar</button>
                        </form>
                    </div>
                </div></div>


 <div class="ibox">
                    <div class="ibox-head">
                        <div class="ibox-title">Listado de Regimen</div>
                    </div>
                    <div class="ibox-body">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>

                                    <tr>
                                       
                                        <th>Id</th>
                                        <th>Tipo</th>
                                       
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                           @foreach($regimens as $reg)
                                    <tr>
                                       
                                        
                                        <td>{{$reg->id}}</td>
                                      <td>{{$reg->tipo}}</td>
                                        <td>
                                           
                                          
                                            <!-- Button trigger modal -->
<button type="button" class="btn btn-default btn-xs" data-toggle="modal" data-target="#exampleModalCenter{{$reg->id}}" >
    <i class="fa fa-pencil font-14"></i>
</button>

<!-- Modal -->
<div class="modal fade" id="exampleModalCenter{{$reg->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Actualizar datos del regimen</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <div class="ibox-body">
       <form class="form-horizontal" action="regimens/{{$reg->id}}" method="post">
               @csrf
                @method('put')
                  <div class="form-group row">
                 <label class="col-sm-2 col-form-label">Regimen</label>
                  <div class="col-sm-10">
                   <input class="form-control" name="nombre" type="text" value="{{$reg->tipo}}">
                         </div>
                        </div>
                                  
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Actualizar</button>
      </div>
                                  
                                </form>
                            </div>
      </div>
     
    </div>
  </div>
</div>    

 <a href="javascript: document.getElementById('delete{{$reg->id}}').submit()" class="btn btn-default btn-xs" data-toggle="tooltip" data-original-title="Eliminar"
 onclick="return confirm('deseas borrar?')"> <i class="fa fa-trash font-14"></i></a>

                                                  <form id="delete{{$reg->id}}" action="regimens/{{$reg->id}}" method="POST">
                                                    @csrf
                                                    @method('delete')
                                                   
                                                  </form>
                                        </td>
                                    </tr>
                                  @endforeach
                                  
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>                
@endsection                