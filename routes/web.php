<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\EstadoController;
use App\Http\Controllers\MunicipioController;
use App\Http\Controllers\RegimenController;
use App\Http\Controllers\ClienteController;
use App\Http\Controllers\PagoController;

/*
digamos que ya estoy logeado y me voy a la ruta del login  entonces con guest vamos a redireccionar al dahsboard para evitar inicar sesion varias veces,
para modificar esta vista es en app/provider/RouteServiceProvider y cambiamos el valor de la siguiente vatriable
public const HOME = '/home';
*/
Route::get('/', function () {
    return view('login');
})->middleware('guest');

//enviamos los datos para validar
Route::post('login-form', [UserController::class, 'valUser']);

/*
protegemos nuestras rutas para que si el usuario no esta logeado lo redirija al login
para modificar la ruta del login podemos ir a
app/http/middleware/Authenticate
y modificamos el    return '/';
*/
Route::resource('home',PagoController::class)->middleware('auth');


Route::resource('estados',EstadoController::class)->middleware('auth');
Route::resource('municipios',MunicipioController::class)->middleware('auth');
Route::resource('regimens',RegimenController::class)->middleware('auth');

Route::resource('clientes',ClienteController::class)->middleware('auth');

Route::get('/showFiles/{file}',[ClienteController::class,'showFile'])->middleware('auth');

Route::delete('/delete-file/{file}',[ClienteController::class,'delFile'])->name('delfi')->middleware('auth');

Route::post('logout', [UserController::class, 'logout']);
