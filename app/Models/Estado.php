<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Estado extends Model
{
    use HasFactory;
    //un estado puede tener muchos municipios
    public function municipio(){
        return $this->hasMany('App\Models\Municipio');
    }
}
