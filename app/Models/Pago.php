<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pago extends Model
{
    use HasFactory;

      //varios pagos pueden ser de un cliente
    public function cliente(){
        return $this->belongsTo('App\Models\Cliente');
    }
}
