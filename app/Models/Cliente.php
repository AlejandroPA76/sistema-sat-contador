<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
    use HasFactory;
    //varios clientes pueden ser registrados en un municipio
    public function municipio(){
        return $this->belongsTo('App\Models\Municipio');
    }
//tuve que crear esta relacion porque el cliente al registrarse tiene un municipio y a su vez el municipio permanece a un estado
    public function estado(){
        return $this->hasMany('App\Models\Estado');
    }

      //varios clientes pueden ser registrados en un regimen
    public function regimen(){
        return $this->belongsTo('App\Models\Regimen');
    }

   //varios pagos pueden ser de un cliente
    public function pago(){
        return $this->hasMany('App\Models\Pago');
    }

     public function archivo(){
        return $this->hasMany('App\Models\Archivo');
    }
}
