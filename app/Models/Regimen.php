<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Regimen extends Model
{
    use HasFactory;
    //varios clientes pueden estar registrados en un regimen
    public function cliente(){
        return $this->hasMany('App\Models\Cliente');
    }
}
