<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Municipio extends Model
{
    use HasFactory;

      protected $fillable = [
    
        'nombre',
        
    ];

    //un municipio puede estar en un solo estado
    public function estado(){
        return $this->belongsTo('App\Models\Estado');
    }
//un municipio puede tener varios clientes
    public function cliente(){
        return $this->hasMany('App\Models\Cliente');
    }
}
