<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function valUser(Request $request){
//hacemos la validacion de los campos
      $credencials = request()->validate([
        'email' => ['required', 'email', 'string'], 
        'password' => ['required', 'string'],
            
     ]);
      $remenber = request()->filled('remenber');
      
      /*
    se envia los campos de usuario y contraseña y el remenber que es un boleano 
    para recordar o no la sesion,
    acuerdate que en produccion cambiar la opcion de config/session.php a
     'driver' => env('SESSION_DRIVER', 'cookie'),
      */
      if (Auth::attempt($credencials,$remenber) ) {
        /*regeneramos la sesion cada cierto tiempo en caso de que el usuario no ativo recordar la sesion
        
        */
        request()->session()->regenerate();
      /*el intended es para que si el usuario intentar entrar a otra url que no le pertenece lo redirija al dashboard o a la ruta que corresponda*/
          return redirect()
          ->intended('home');
      }
      return redirect('/');
      


    }

    public function logout(Request $request ){
       Auth::logout();
       $request->session()->invalidate();
       $request->session()->regenerateToken();
        return redirect('/');
    }
}
