<?php

namespace App\Http\Controllers;

use App\Models\Municipio;
use App\Models\Estado;
use Illuminate\Http\Request;

class MunicipioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        /* 
        consulto la lista de municipios para que al momento de crear uno nuevo ya tengamos
        la lista de estados y solo es de seleccionar uno de ellos 
        */
        $estados = Estado::orderBy('created_at', 'desc')->get();
        
        /*
        En el modelo de municipio tengo las relaciones creadas, por ende con with puedo hacer 
        la consulta para obtener todos los municipios y traer la informacion de la tabla 
        de la que tomamos como llave foranea
        */
        $municipios = Municipio::with('estado')->orderBy('created_at', 'desc')->get();
        //$jsonMunicipios = $municipios->toJson();
      //  return $municipios;
       
    return view('municipios.index', compact('estados','municipios'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $newMunicipio = new Municipio;
       $newMunicipio->nombre = $request->input('nombre');
       $newMunicipio->estado_id = $request->input('estado');
       $newMunicipio->save();
       return redirect('/municipios');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Municipio  $municipio
     * @return \Illuminate\Http\Response
     */
    public function show(Municipio $municipio)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Municipio  $municipio
     * @return \Illuminate\Http\Response
     */
    public function edit(Municipio $municipio)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Municipio  $municipio
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Municipio $municipio)
    {
       $uptMunicipio = Municipio::find($municipio->id);
       $uptMunicipio->nombre = $request->input('nombre');
       $uptMunicipio->estado_id = $request->input('estado');
       $uptMunicipio->save();
       return redirect('municipios');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Municipio  $municipio
     * @return \Illuminate\Http\Response
     */
    public function destroy(Municipio $municipio)
    {
        $delMu = Municipio::find($municipio->id);
        $delMu->delete();
        return redirect('municipios');
    }
}
