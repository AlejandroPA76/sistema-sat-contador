<?php

namespace App\Http\Controllers;

use App\Models\Regimen;
use Illuminate\Http\Request;

class RegimenController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    $regimens = Regimen::orderBy('created_at', 'desc')->get();
    return view('regimens.index',compact('regimens'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $newRegimen = new Regimen;
        $newRegimen->tipo = $request->input('name');
        $newRegimen->save();
        return redirect('/regimens');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Regimen  $regimen
     * @return \Illuminate\Http\Response
     */
    public function show(Regimen $regimen)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Regimen  $regimen
     * @return \Illuminate\Http\Response
     */
    public function edit(Regimen $regimen)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Regimen  $regimen
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Regimen $regimen)
    {

      
        $uptRegimen = Regimen::find($regimen->id);
        $uptRegimen->tipo = $request->input('nombre');
        $uptRegimen->save();
        return redirect('regimens');
      

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Regimen  $regimen
     * @return \Illuminate\Http\Response
     */
    public function destroy(Regimen $regimen)
    {
        //return $regimen;
        $delRegimen = Regimen::find($regimen->id);
        $delRegimen ->delete();
        return redirect('regimens');
    }
}
