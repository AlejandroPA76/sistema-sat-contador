<?php

namespace App\Http\Controllers;

use App\Models\Cliente;
use App\Models\Regimen;
use App\Models\Municipio;
use App\Models\Estado;
use App\Models\Archivo;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ClienteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //consultas para generar datos al select
        $regimens = Regimen::orderBy('created_at', 'desc')->get();
        $regions = Municipio::with('estado')->orderBy('created_at', 'desc')->get();


        //para la tabla
        //hice esta consulta porque varios municipios permanecen a un estado y agregue otra relacion de otra tabla de regimen para ver el nombre del regimen
        $clientes = Cliente::with('municipio.estado', 'regimen','archivo')->orderBy('created_at', 'desc')->get();
    

        // Devolver la lista de archivos
       // return $carpetas;
      //  return $clientes;
        
       return view('clientes.index', compact('regimens','regions','clientes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

      //creo al usuario
       $newCliente = new Cliente;
       $newCliente->nombre = $request->input('nombre');
       $newCliente->observacion = $request->input('observacion');
       $newCliente->regimen_id = $request->input('regimen');
       $newCliente->municipio_id = $request->input('region');

       $newCliente->save();
      

    /*   
    con esto obtengo el id que acabo de crear
    $newCliente->id;    
    */
      // 

        //nombro la carpeta el numero del id del cliente que se acaba de registrar
        $nombreCarpeta = $newCliente->id;
        //dentro de la carpeta public hago una carpeta que es donde voy a almacenar los archivos
        $rutaCarpeta = 'public/' . $nombreCarpeta;
        //comprobamos si la carpeta existe
        if (!Storage::exists($rutaCarpeta)) {
            //creamos la carpeta
         Storage::makeDirectory($rutaCarpeta);
         //return redirect('/clientes');
        }
       //si existe algun archivo que guardar
       if ($request->hasFile('archivos')) {
          //recorremos los archivos
         foreach ($request->file('archivos') as $file) {
            //obtenemos el nombre original del archivo
            $nombreArchivo = $file->getClientOriginalName();
            //guardamos los archivos
            //primer parametro: la ruta de donde se va a guardar el archivo
            //segundo parametro: el archivo de dependiendo el bucle
            //tercer parametro: el nombre con que se va a guardar 
            Storage::putFileAs($rutaCarpeta, $file, $nombreArchivo);
            print("". $rutaCarpeta . "/" .$nombreArchivo);
            //guardo en la base de datos la ubicacion de los archivos
             $newArchivo = new Archivo;
             $newArchivo->nombre = $nombreArchivo;
            $newArchivo->directorio = $rutaCarpeta."/". $nombreArchivo;
            $newArchivo->cliente_id = $newCliente->id;
            $newArchivo->save();
         }
       
       }
       
     
        return redirect('clientes');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Cliente  $cliente
     * @return \Illuminate\Http\Response
     */
    public function show(Cliente $cliente)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Cliente  $cliente
     * @return \Illuminate\Http\Response
     */
    public function edit(Cliente $cliente)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Cliente  $cliente
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Cliente $cliente)
    {

     $uptCliente = Cliente::find($cliente->id);
     $uptCliente->nombre = $request->input('nombre');
     $uptCliente->regimen_id = $request->input('regimen');
     $uptCliente->observacion = $request->input('observacion');     
     $uptCliente->municipio_id = $request->input('region');
     $uptCliente->save();

       //nombro la carpeta el numero del id del cliente que se acaba de registrar
        $nombreCarpeta = $cliente->id;
        //dentro de la carpeta public hago una carpeta que es donde voy a almacenar los archivos
        $rutaCarpeta = 'public/' . $nombreCarpeta;
        //comprobamos si la carpeta existe
        if (Storage::exists($rutaCarpeta)) {
     
//var_dump($request->archivos);

           
 
           //si existe algun archivo que guardar
       if ($request->hasFile('archivos')) {
       // print("si existe");
         
          //recorremos los archivos agregados en el input
         foreach ($request->file('archivos') as $file) {
           
            //obtenemos el nombre original del archivo
            $nombreArchivo = $file->getClientOriginalName();
      //recorro el contenido de la carpeta
        $archivos = Storage::files($rutaCarpeta);
        //print_r($archivos);
        //iniciamos la variable en ceros
        $valor = 0;
        //recorremos para comparar el archivo cargado con cada uno guardado en el storage por su nombre
        foreach ($archivos as $arch) {
           //con basename quito toda la ruta y solo me quedo con el puro nombre del archivo echo basename($arch . "<br>");
            //aqui voy a compara si el usuario ya tiene un  archivo con el mismo nombre, asi que para reemplazar necesita cambiar el nombre del archivo por otro    
          //  print($nombreArchivo . "<br>");

            //si el archivo cargado es igual al del storage
            //la variable valor le asignamos 1

            //basename es el nombre del archivo del storage 
            //$nombreArchivo es el nombre del archivo del input del bootstrap
            if(basename($arch) == $nombreArchivo){
                $valor = 1;
            }
        }
        //ya fuera del foreach guardo, ya que si no voy a guardar el mismo archivo varias veces en la base de datos
        //fuera ya checamos como quedo el valor de la variable valor
        //si valor quedo en ceros procedemos a guardar el archivo que esta sin repetir
           if ($valor == 0) {
            
            //guardo en la base de datos la ubicacion de los archivos
            $newArchivo = new Archivo;
            $newArchivo->nombre = $nombreArchivo;
            $newArchivo->directorio = $rutaCarpeta."/". $nombreArchivo;
            $newArchivo->cliente_id = $cliente->id;
            $newArchivo->save();
           }
        Storage::putFileAs($rutaCarpeta, $file, $nombreArchivo);
            
         }

   
       }
      


        }



     return redirect('clientes');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Cliente  $cliente
     * @return \Illuminate\Http\Response
     */
    public function destroy(Cliente $cliente)
    {
        //busco en la carpeta public la carpeta con el nombre del id del cliente 
        $carpeta = 'public/' . $cliente->id;
        //si existe
        if (Storage::exists($carpeta)) {
            // Borrar la carpeta y su contenido
            Storage::deleteDirectory($carpeta);
            
        }
        $delCliente = Cliente::find($cliente->id);
        $delCliente->delete();
        return redirect('clientes');

        //return $cliente;
    }


    public function showFile($arch)
    {
        //return Storage::download($request);
       //busco el archivo en la base de datos con el id
         $downFile = Archivo::find($arch);
         //ahora si solo extraemos el puro nombre
        $nombreArchivo = basename($downFile->directorio);
        //retornamos la descarga sin agregar la ubicacion en archivos txt
        return Storage::download($downFile->directorio, $nombreArchivo);
      /*
        Route::get('/uploads/{file}', function ($file) {
    return Storage::response("uploads/$file");
});
        */
        /*
         $rutaCarpeta = 'public/' . $request->id;
        //comprobamos si la carpeta existe
        if (Storage::exists($rutaCarpeta)) {
         //echo "si exitse";
            $archivos = Storage::allFiles($rutaCarpeta);
            return $archivos;
           /*
            foreach ($archivos as $archivo) {
                //echo $archivo . '<br>';
                //retiro la ruta de los archivos y solo conservo el nombre de los archivos existentes en esa carpeta
                $name = str_replace("public/$request->id/", "", $archivo);
                echo $name . '<br>';
            }
        }else{
            echo "no exite";
        }

        */
    }

//con esto recopilo el request y el id 
    public function delFile(Request $request,$id){
//buscamos en la base de datos por el id y con eso obtenemos la direccion donde se encuentra el archivo
    $delFile = Archivo::findorFail($id);
    $nombreArchivo = $delFile->directorio;
    print($nombreArchivo);
 if (Storage::exists($nombreArchivo)) {
        // Elimina el archivo
        Storage::delete($nombreArchivo);
        $delFile->delete();
        
    }
    return redirect('clientes' );

    //return $request->model;
    // return $id;  
    //return redirect('clientes' );
    }
}
